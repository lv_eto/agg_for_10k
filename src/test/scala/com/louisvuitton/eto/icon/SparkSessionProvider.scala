package com.louisvuitton.eto.icon

import org.apache.spark.sql.SparkSession

trait SparkSessionProvider {
  @transient
  val sparkSession = SparkSession
    .builder()
    .appName("SparkSession for unit tests")
    .master("local")
    .getOrCreate()
}
