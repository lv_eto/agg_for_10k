package com.louisvuitton.eto.icon


import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.scalatest.{FlatSpec, FunSuite, Matchers}

abstract class AggregateContactActiveSpec extends  UnitSpec with DataFrameSuiteBase with Matchers with SparkSessionProvider {

  import spark.implicits._


  "Ref dfAggActive " should "have the same as dfAgregateContactActive table" in {

    val dfAggActive = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", ";")
      .load(this.getClass.getResource("Agg_Contact_Active.csv").getPath)


    val dfTransaction = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", ";")
      .load(this.getClass.getResource("Contact.csv").getPath)

    val dfDimStore = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", ";")
      .load(this.getClass.getResource("Contact.csv").getPath)

    val dfDimTypeClient = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", ";")
      .load(this.getClass.getResource("Contact.csv").getPath)

    val dfRefSalePerson = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", ";")
      .load(this.getClass.getResource("Contact.csv").getPath)

    val dfContact = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", ";")
      .load(this.getClass.getResource("Contact.csv").getPath)

      .join(dfRefSalePerson.filter(!$"CA_Id".isin("0050H00000DOct0QAD")),
        $"salesperson_unique_id" <=> $"CA_WW_EmployeeNumber",
        "left")
      .join(dfDimStore.select("STORE_CODE", "STORE_COUNTRY_ISO_ID"),
        $"STORE_CODE" <=> $"client_attached_channel_code", "left")
      .join(dfDimTypeClient.withColumnRenamed("Libelle", "Libelle_Type_Contact"),
        $"typology_id" <=> $"contact_type_id", "left")


    val listTypeClient = List(0, 1, 2, 3, 4, 5, 6, 9)

    val dfSelContactTrs = dfTransaction.filter($"dream_id".isNotNull && $"transaction_id".isNotNull)
      .join(dfContact.withColumn("typology_clt", substring($"typology_id", 0, 2).cast(IntegerType))
        .filter($"typology_clt".isin(listTypeClient: _*)), Seq("dream_id"), "inner")
      .filter($"transaction_date_yyyymmdd".isNotNull)
      .withColumn("Flag12MR", when($"transaction_date_yyyymmdd".isNotNull, 1)
        .otherwise(0))
      .withColumn("FlagFYY1", when($"transaction_date_year".isNotNull, 1)
        .otherwise(0))
      .withColumn("FlagYTM", when($"transaction_date_yyyymmdd".isNotNull, 1)
        .otherwise(0))
      .withColumn("FlagYTMN1", when($"transaction_date_yyyymmdd".isNotNull, 1)
        .otherwise(0))
      .withColumn("Flag12MRN1", when($"transaction_date_yyyymmdd".isNotNull, 1)
        .otherwise(0))


    val dfAgregateContactActive = dfSelContactTrs.groupBy("dream_id", "client_attached_channel_code",
      "Libelle_Type_Contact", "STORE_COUNTRY_ISO_ID",
      "main_postal_country", "CA_Id", "CA_owner_name")
      .agg(sum(when($"Flag12MR" === 1, $"line_tax_incl_Euro").otherwise(0))
        .alias("Turnover_TTC_12MR_Euro"),
        sum(when($"Flag12MR" === 1, $"line_tax_incl_Dollar").otherwise(0))
          .alias("Turnover_TTC_12MR_Dollar"),
        sum(when($"Flag12MR" === 1, $"line_tax_incl_Yen").otherwise(0))
          .alias("Turnover_TTC_12MR_Yen"),
        sum(when($"Flag12MR" === 1, $"line_tax_incl_Yuan").otherwise(0))
          .alias("Turnover_TTC_12MR_Yuan"),
        sum(when($"Flag12MR" === 1, $"line_tax_incl_Livre").otherwise(0))
          .alias("Turnover_TTC_12MR_Pound"),
        sum(when($"Flag12MR" === 1, $"line_tax_incl_Won").otherwise(0))
          .alias("Turnover_TTC_12MR_Won"),
        sum(when($"Flag12MR" === 1, $"line_tax_incl_Lire").otherwise(0))
          .alias("Turnover_TTC_12MR_Lira"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Euro").otherwise(0))
          .alias("Turnover_TTC_FYY1_Euro"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Dollar").otherwise(0))
          .alias("Turnover_TTC_FYY1_Dollar"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Yen").otherwise(0))
          .alias("Turnover_TTC_FYY1_Yen"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Yuan").otherwise(0))
          .alias("Turnover_TTC_FYY1_Yuan"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Livre").otherwise(0))
          .alias("Turnover_TTC_FYY1_Pound"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Won").otherwise(0))
          .alias("Turnover_TTC_FYY1_Won"),
        sum(when($"FlagFYY1" === 1, $"line_tax_incl_Lire").otherwise(0))
          .alias("Turnover_TTC_FYY1_Lira"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Euro").otherwise(0))
          .alias("Turnover_TTC_YTM_Euro"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Dollar").otherwise(0))
          .alias("Turnover_TTC_YTM_Dollar"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Yen").otherwise(0))
          .alias("Turnover_TTC_YTM_Yen"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Yuan").otherwise(0))
          .alias("Turnover_TTC_YTM_Yuan"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Livre").otherwise(0))
          .alias("Turnover_TTC_YTM_Pound"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Won").otherwise(0))
          .alias("Turnover_TTC_YTM_Won"),
        sum(when($"FlagYTM" === 1, $"line_tax_incl_Lire").otherwise(0))
          .alias("Turnover_TTC_YTM_Lira"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Euro").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Euro"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Dollar").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Dollar"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Yen").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Yen"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Yuan").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Yuan"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Livre").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Pound"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Won").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Won"),
        sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Lire").otherwise(0))
          .alias("Turnover_TTC_YTMY1_Lira"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Euro").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Euro"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Dollar").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Dollar"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Yen").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Yen"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Yuan").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Yuan"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Livre").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Pound"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Won").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Won"),
        sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Lire").otherwise(0))
          .alias("Turnover_TTC_12MRN1_Lira"))
      .withColumn("Type_contact", when($"STORE_COUNTRY_ISO_ID" === $"main_postal_country", "Local")
        .otherwise("Non Local"))
      .withColumn("annee", lit(2020))
      .withColumn("mois", lit(6))
      .withColumnRenamed("main_postal_country", "Id_Pays")
      .withColumnRenamed("CA_Id", "CA_SF_Code")
      .withColumnRenamed("CA_owner_name", "CA_NAME")
      .withColumnRenamed("dream_id", "id_contact")
      .withColumn("Date_Creation", current_date())
      .withColumnRenamed("Libelle_Type_Contact", "Typology_contact")
      .withColumnRenamed("client_attached_channel_code", "Id_Mag_Rattachement")
      .select("id_contact", "annee", "mois", "Type_contact",
        "Id_Mag_Rattachement", "Id_Pays", "CA_SF_Code", "CA_NAME",
        "Turnover_TTC_12MR_Euro", "Turnover_TTC_12MR_Dollar", "Turnover_TTC_12MR_Yen",
        "Turnover_TTC_12MR_Yuan", "Turnover_TTC_12MR_Pound", "Turnover_TTC_12MR_Won",
        "Turnover_TTC_12MR_Lira", "Turnover_TTC_FYY1_Euro", "Turnover_TTC_FYY1_Dollar",
        "Turnover_TTC_FYY1_Yen", "Turnover_TTC_FYY1_Yuan", "Turnover_TTC_FYY1_Pound",
        "Turnover_TTC_FYY1_Won", "Turnover_TTC_FYY1_Lira", "Turnover_TTC_YTM_Euro",
        "Turnover_TTC_YTM_Dollar", "Turnover_TTC_YTM_Yen", "Turnover_TTC_YTM_Yuan",
        "Turnover_TTC_YTM_Pound", "Turnover_TTC_YTM_Won", "Turnover_TTC_YTM_Lira",
        "Turnover_TTC_YTMY1_Euro", "Turnover_TTC_YTMY1_Dollar", "Turnover_TTC_YTMY1_Yen",
        "Turnover_TTC_YTMY1_Yuan", "Turnover_TTC_YTMY1_Pound", "Turnover_TTC_YTMY1_Won",
        "Turnover_TTC_YTMY1_Lira", "Turnover_TTC_12MRN1_Euro", "Turnover_TTC_12MRN1_Dollar",
        "Turnover_TTC_12MRN1_Yen", "Turnover_TTC_12MRN1_Yuan", "Turnover_TTC_12MRN1_Pound",
        "Turnover_TTC_12MRN1_Won", "Turnover_TTC_12MRN1_Lira", "Date_Creation", "Typology_contact")


    assertDataFrameApproximateEquals(dfAggActive, dfAgregateContactActive,100)

  }
}