package com.louisvuitton.eto.icon

import org.scalatest._

@transient
abstract class UnitSpec extends FlatSpec with Matchers with
  OptionValues with Inside with Inspectors

