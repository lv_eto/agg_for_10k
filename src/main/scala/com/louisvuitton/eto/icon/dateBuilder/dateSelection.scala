package com.louisvuitton.eto.icon.dateBuilder

import org.apache.spark.sql.SparkSession
//import org.apache.spark.sql.functions.{col, current_date,when,date_format, expr, regexp_replace}
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.functions._


class dateSelection(spark: SparkSession, date_input : String) {

  import spark.implicits._
  var date_init = date_input
  val df_periode = Seq(
    (s"$date_init", "Month_min", "Month_max", "YTM_min", "YTM_max", "12MR_min", "12MR_max")).toDF("Current_date", "Month_min", "Month_max",
    "YTM_min", "YTM_max", "12MR_min", "12MR_max")
    .withColumn("Current_date", date_format(col("Current_date").cast("date"), "YYYY-MM-dd"))
    .withColumn("Current_dateN1", $"Current_date".cast("date") + expr("INTERVAL -1 YEAR"))
    .withColumn("Month_min", (date_format(col("Current_date").cast("date") - expr("INTERVAL 1 MONTH"),
      "yyyy-MM-28") + expr("INTERVAL 1 DAY")).cast(DateType))
    .withColumn("Month_max", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28"))
    .withColumn("YTM_min", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-01-01"))
    .withColumn("YTM_max", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28"))
    .withColumn("YTM_minN1", date_format(col("Current_dateN1").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-01-01"))
    .withColumn("YTM_maxN1", date_format(col("Current_dateN1").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28"))
    .withColumn("12MR_min", (date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28") - expr("INTERVAL 1 YEAR") + expr("INTERVAL 1 DAY")).cast(DateType))
    .withColumn("12MR_max", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28"))
    .withColumn("12MR_minN1", (date_format(col("Current_dateN1").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28") - expr("INTERVAL 1 YEAR") + expr("INTERVAL 1 DAY")).cast(DateType))
    .withColumn("12MR_maxN1", date_format(col("Current_dateN1").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28"))
    .withColumn("2Y_min", (date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28") - expr("INTERVAL 2 YEAR")).cast(DateType))
    .withColumn("2Y_max", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"),
      "yyyy-MM-28"))
    .withColumn("Current_dateN1", regexp_replace(date_format(col("Current_dateN1"),"YYYY"), "-", ""))
    .withColumn("12MR_min", regexp_replace(col("12MR_min"), "-", ""))
    .withColumn("12MR_max", regexp_replace(col("12MR_max"), "-", ""))
    .withColumn("12MR_minN1", regexp_replace(col("12MR_minN1"), "-", ""))
    .withColumn("12MR_maxN1", regexp_replace(col("12MR_maxN1"), "-", ""))
    .withColumn("Month_min", regexp_replace(col("Month_min"), "-", ""))
    .withColumn("Month_max", regexp_replace(col("Month_max"), "-", ""))
    .withColumn("YTM_min", regexp_replace(col("YTM_min"), "-", ""))
    .withColumn("YTM_max", regexp_replace(col("YTM_max"), "-", ""))
    .withColumn("YTM_minN1", regexp_replace(col("YTM_minN1"), "-", ""))
    .withColumn("YTM_maxN1", regexp_replace(col("YTM_maxN1"), "-", ""))
    .withColumn("2Y_min", regexp_replace(col("2Y_min"), "-", ""))
    .withColumn("2Y_max", regexp_replace(col("2Y_max"), "-", ""))
    .withColumn("mois_calcul", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"), "MM"))
    .withColumn("annee_calcul", date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"), "yyyy"))
    .withColumn("YTM_min", when(date_format(col("Current_date").cast("date") - expr("INTERVAL 0 MONTH"), "MM")
      .cast("Int") === 12, $"12MR_min")
      .otherwise($"YTM_min"))


  var month_min = df_periode.select("Month_min").collect()(0)(0)
  var month_max = df_periode.select("Month_max").collect()(0)(0)

  var YTM_min = df_periode.select("YTM_min").collect()(0)(0)
  var YTM_max = df_periode.select("YTM_max").collect()(0)(0)

  var YTM_minN1 = df_periode.select("YTM_minN1").collect()(0)(0)
  var YTM_maxN1 = df_periode.select("YTM_maxN1").collect()(0)(0)

  var date12MR_min = df_periode.select("12MR_min").collect()(0)(0)
  var date12MR_max = df_periode.select("12MR_max").collect()(0)(0)

  var date12MR_minN1 = df_periode.select("12MR_minN1").collect()(0)(0)
  var date12MR_maxN1 = df_periode.select("12MR_maxN1").collect()(0)(0)

  val Datedeb_2y = df_periode.select("2Y_min").collect()(0)(0)
  val Datefin_2y = df_periode.select("2Y_max").collect()(0)(0)

  val month_calcul = df_periode.select("mois_calcul").collect()(0)(0)
  val year_calcul = df_periode.select("annee_calcul").collect()(0)(0)
  val previous_year = df_periode.select("Current_dateN1").collect()(0)(0)


}
