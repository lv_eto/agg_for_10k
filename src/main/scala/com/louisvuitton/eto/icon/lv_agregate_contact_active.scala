package com.louisvuitton.eto.icon

import com.typesafe.config.ConfigFactory
import org.apache.logging.log4j.scala.Logging
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType

object lv_agregate_contact_active extends Logging {

  def main(args: Array[String]): Unit = {
    logger.getClass()
    val env = args(0)

    if (args.length < 2) {

      logger.error("Paramètre manquant")
    }
    else if (!(env == "DEV" || env == "DEV_LOCAL" || env == "PRD")) {

      logger.error("Le paramètre renseigné n'est pas le bon")
    }

    else {
      logger.error("******************************* GO!!!! ******************************************")

      val spark = SparkSession.builder.master(ConfigFactory.load()
        .getString(s"conf_lv_active.$env.session"))
        .appName("lv_agg_contact_active")
        .config("spark.driver.memory", "2g")
        .config("spark.ui.retainedJobs", "10000")
        .config("spark.ui.retainedStages", "10000")
        .config("spark.shuffle.service.enabled", "true")
        .config("spark.dynamicAllocation.enabled", "true")
        .getOrCreate()
      import spark.implicits._
      val t0 = System.nanoTime()

      logger.error("*******************************CHARGEMENT DES TABLES******************************************")

      val dfTransaction   = spark.read.parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_transaction"))
      val dfDimStore      = spark.read.parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_store"))
      val dfDimTypeClient = spark.read.parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_type_client"))
      val dfRefSalePerson =
        spark.read.parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_ref_sale_person"))
      val dfHistoGreyMarket = spark.read
        .parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_grey_market"))
        .filter($"gm_current_status" === 1)

      val dfContact = spark.read
        .parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_contact"))
        .filter($"is_significant_client" =!=0)
        .join(dfRefSalePerson.filter(!$"CA_Id".isin("0050H00000DOct0QAD")),
          $"salesperson_unique_id" <=> $"CA_WW_EmployeeNumber",
          "left")
        .join(dfDimStore.select("STORE_CODE", "STORE_COUNTRY_ISO_ID"),
          $"STORE_CODE" <=> $"client_attached_channel_code",
          "left")
        .join(dfDimTypeClient.withColumnRenamed("Libelle", "Libelle_Type_Contact"),
          $"typology_id" <=> $"contact_type_id",
          "left")
        .join(dfHistoGreyMarket.select("dream_id","gm_status"), Seq("dream_id"), "left")
        .filter($"gm_status" =!= "Red" || $"gm_status".isNull|| $"gm_status".isin("Green","Red","Orange"))
          .drop("gm_status")


      logger.error("<==============================================================================>")
      logger.error("<=========================== FLAG POUR CALCUL AGG==============================>")
      logger.error("<==============================================================================>")

      val date = new dateBuilder.dateSelection(spark,args(1))

      val listTypeClient = List(0, 1, 2, 3, 4, 5, 6, 9)

      val dfSelContactTrs = dfTransaction.filter($"dream_id".isNotNull && $"transaction_id".isNotNull)
        .join(dfContact.withColumn("typology_clt", substring($"typology_id", 0, 2).cast(IntegerType))
          .filter($"typology_clt".isin(listTypeClient: _*)), Seq("dream_id"), "inner")
        .filter($"transaction_date_yyyymmdd".between(date.Datedeb_2y, date.Datefin_2y))
        .withColumn("Flag12MR", when($"transaction_date_yyyymmdd".between(date.date12MR_min, date.date12MR_max), 1)
          .otherwise(0))
        .withColumn("FlagFYY1", when($"transaction_date_year"===date.previous_year, 1)
          .otherwise(0))
        .withColumn("FlagYTM", when($"transaction_date_yyyymmdd".between(date.YTM_min, date.YTM_max), 1)
          .otherwise(0))
        .withColumn("FlagYTMN1", when($"transaction_date_yyyymmdd".between(date.YTM_minN1, date.YTM_maxN1), 1)
          .otherwise(0))
        .withColumn("Flag12MRN1", when($"transaction_date_yyyymmdd".between(date.date12MR_minN1, date.date12MR_maxN1), 1)
          .otherwise(0))


      logger.error("<===================================================================================>")
      logger.error("<==============================CALCUL DES AGG ======================================>")
      logger.error("<===================================================================================>")

      val dfAgregateContactActive = dfSelContactTrs.groupBy("dream_id", "client_attached_channel_code",
        "Libelle_Type_Contact", "STORE_COUNTRY_ISO_ID",
        "main_postal_country", "CA_Id", "CA_owner_name")
        .agg(sum(when($"Flag12MR" === 1, $"line_tax_incl_Euro").otherwise(0))
          .alias("Turnover_TTC_12MR_Euro"),
          sum(when($"Flag12MR" === 1, $"line_tax_incl_Dollar").otherwise(0))
            .alias("Turnover_TTC_12MR_Dollar"),
          sum(when($"Flag12MR" === 1, $"line_tax_incl_Yen").otherwise(0))
            .alias("Turnover_TTC_12MR_Yen"),
          sum(when($"Flag12MR" === 1, $"line_tax_incl_Yuan").otherwise(0))
            .alias("Turnover_TTC_12MR_Yuan"),
          sum(when($"Flag12MR" === 1, $"line_tax_incl_Livre").otherwise(0))
            .alias("Turnover_TTC_12MR_Pound"),
          sum(when($"Flag12MR" === 1, $"line_tax_incl_Won").otherwise(0))
            .alias("Turnover_TTC_12MR_Won"),
          sum(when($"Flag12MR" === 1, $"line_tax_incl_Lire").otherwise(0))
            .alias("Turnover_TTC_12MR_Lira"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Euro").otherwise(0))
            .alias("Turnover_TTC_FYY1_Euro"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Dollar").otherwise(0))
            .alias("Turnover_TTC_FYY1_Dollar"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Yen").otherwise(0))
            .alias("Turnover_TTC_FYY1_Yen"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Yuan").otherwise(0))
            .alias("Turnover_TTC_FYY1_Yuan"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Livre").otherwise(0))
            .alias("Turnover_TTC_FYY1_Pound"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Won").otherwise(0))
            .alias("Turnover_TTC_FYY1_Won"),
          sum(when($"FlagFYY1" === 1, $"line_tax_incl_Lire").otherwise(0))
            .alias("Turnover_TTC_FYY1_Lira"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Euro").otherwise(0))
            .alias("Turnover_TTC_YTM_Euro"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Dollar").otherwise(0))
            .alias("Turnover_TTC_YTM_Dollar"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Yen").otherwise(0))
            .alias("Turnover_TTC_YTM_Yen"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Yuan").otherwise(0))
            .alias("Turnover_TTC_YTM_Yuan"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Livre").otherwise(0))
            .alias("Turnover_TTC_YTM_Pound"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Won").otherwise(0))
            .alias("Turnover_TTC_YTM_Won"),
          sum(when($"FlagYTM" === 1, $"line_tax_incl_Lire").otherwise(0))
            .alias("Turnover_TTC_YTM_Lira"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Euro").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Euro"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Dollar").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Dollar"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Yen").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Yen"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Yuan").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Yuan"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Livre").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Pound"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Won").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Won"),
          sum(when($"FlagYTMN1" === 1, $"line_tax_incl_Lire").otherwise(0))
            .alias("Turnover_TTC_YTMY1_Lira"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Euro").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Euro"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Dollar").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Dollar"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Yen").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Yen"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Yuan").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Yuan"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Livre").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Pound"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Won").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Won"),
          sum(when($"Flag12MRN1" === 1, $"line_tax_incl_Lire").otherwise(0))
            .alias("Turnover_TTC_12MRN1_Lira"))
        .withColumn("Type_contact", when($"STORE_COUNTRY_ISO_ID" === $"main_postal_country", "Local")
          .otherwise("Non Local"))
        .withColumn("Type_contact",when($"main_postal_country"==="MCO" && $"STORE_COUNTRY_ISO_ID"==="FRA","Local")
          .otherwise($"Type_contact"))
        .withColumn("Type_contact",when($"main_postal_country"==="PRI" && $"STORE_COUNTRY_ISO_ID"==="USA","Local")
          .otherwise($"Type_contact"))
        .withColumn("annee", lit(date.year_calcul))
        .withColumn("mois", lit(date.month_calcul))
        .withColumnRenamed("main_postal_country", "Id_Pays")
        .withColumnRenamed("CA_Id", "CA_SF_Code")
        .withColumnRenamed("CA_owner_name", "CA_NAME")
        .withColumnRenamed("dream_id", "id_contact")
        .withColumn("Date_Creation", current_date())
        .withColumnRenamed("Libelle_Type_Contact", "Typology_contact")
        .withColumnRenamed("client_attached_channel_code", "Id_Mag_Rattachement")
        .select("id_contact", "annee", "mois", "Type_contact",
          "Id_Mag_Rattachement", "Id_Pays", "CA_SF_Code", "CA_NAME",
          "Turnover_TTC_12MR_Euro", "Turnover_TTC_12MR_Dollar", "Turnover_TTC_12MR_Yen",
          "Turnover_TTC_12MR_Yuan", "Turnover_TTC_12MR_Pound", "Turnover_TTC_12MR_Won",
          "Turnover_TTC_12MR_Lira", "Turnover_TTC_FYY1_Euro", "Turnover_TTC_FYY1_Dollar",
          "Turnover_TTC_FYY1_Yen", "Turnover_TTC_FYY1_Yuan", "Turnover_TTC_FYY1_Pound",
          "Turnover_TTC_FYY1_Won", "Turnover_TTC_FYY1_Lira", "Turnover_TTC_YTM_Euro",
          "Turnover_TTC_YTM_Dollar", "Turnover_TTC_YTM_Yen", "Turnover_TTC_YTM_Yuan",
          "Turnover_TTC_YTM_Pound", "Turnover_TTC_YTM_Won", "Turnover_TTC_YTM_Lira",
          "Turnover_TTC_YTMY1_Euro", "Turnover_TTC_YTMY1_Dollar", "Turnover_TTC_YTMY1_Yen",
          "Turnover_TTC_YTMY1_Yuan", "Turnover_TTC_YTMY1_Pound", "Turnover_TTC_YTMY1_Won",
          "Turnover_TTC_YTMY1_Lira", "Turnover_TTC_12MRN1_Euro", "Turnover_TTC_12MRN1_Dollar",
          "Turnover_TTC_12MRN1_Yen", "Turnover_TTC_12MRN1_Yuan", "Turnover_TTC_12MRN1_Pound",
          "Turnover_TTC_12MRN1_Won", "Turnover_TTC_12MRN1_Lira", "Date_Creation", "Typology_contact")


      dfAgregateContactActive.write.mode("overwrite")
        .parquet(ConfigFactory.load().getString(s"conf_lv_active.$env.loc_agg_contact_active"))


    }

  }

}
