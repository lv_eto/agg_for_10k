
name := "lv_agregate_contact_avtive"

version := "0.1"

scalaVersion := "2.11.12"


libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "com.typesafe" % "config" % "1.3.4",
  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "com.typesafe" % "config" % "1.3.4",
  "org.apache.hadoop" % "hadoop-common" % "2.7.0",
  "org.apache.logging.log4j" % "log4j" % "2.12.1",
  "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0",
  "org.apache.logging.log4j" % "log4j-api" % "2.11.0",
  "com.github.nscala-time" %% "nscala-time" % "2.22.0",
  "org.apache.logging.log4j" % "log4j-core" % "2.11.0",
  "com.holdenkarau" %% "spark-testing-base" % "2.1.0_0.12.0" % "test",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
)

assemblyMergeStrategy in assembly := {
  case PathList("org", "apache", "spark", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "hadoop", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "parquet", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "commons", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "commons","collections", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "commons","beanutils", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "commons","logging", xs @ _*) => MergeStrategy.last
  case PathList("org", "apache", "commons","logging","converters", xs @ _*) => MergeStrategy.last
  case PathList("org","slf4j", xs @ _*) => MergeStrategy.last
  case PathList("org", "scalatest", xs @ _*) => MergeStrategy.last
  case PathList("org.datanucleus", xs @ _*) => MergeStrategy.last
  case PathList("ivy2","org.datanucleus", xs @ _*) => MergeStrategy.last
  case PathList("ivy2","org","apache","parquet", xs @ _*) => MergeStrategy.last
  case PathList("ivy2","org","datanucleus", xs @ _*) => MergeStrategy.last
  case PathList("ivy2","com","twitter", xs @ _*) => MergeStrategy.last
  case PathList("ivy2","cache","common-beanutils", xs @ _*)=>MergeStrategy.last
  case PathList("ivy", xs @ _*) => MergeStrategy.last
  case PathList("org.apache.parquet", xs @ _*) => MergeStrategy.last
  case PathList("com.twitter", xs @ _*) => MergeStrategy.last
  case PathList("com", "databricks", xs @ _*) => MergeStrategy.last
  case PathList("com", "github", xs @ _*) => MergeStrategy.last
  case PathList("com","google", xs @ _*) => MergeStrategy.last
  case PathList("com","esotericsoftware", xs @ _*) => MergeStrategy.last
  case PathList("javax", xs @ _*) => MergeStrategy.last
  case PathList("org", "aop", xs @ _*)     => MergeStrategy.last
  case PathList("META-INF", xs @ _*) =>
    (xs map {_.toLowerCase}) match {
      case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) => MergeStrategy.discard
      case _ => MergeStrategy.discard
    }
  case _ => MergeStrategy.last
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}